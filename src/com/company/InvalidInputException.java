package com.company;

/**
 * This is a class which defines custom exception that is thrown when the user input is incorrect.
 */
public class InvalidInputException extends RuntimeException {
    public InvalidInputException() {
        super("Your input is wrong. Please enter a valid one!");
    }
}
