package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a class that contains methods that are used in connection with the changing cells.
 * It serves as a helper class.
 */
public class Helper {
    private static final int[][] directions = new int[][]{{-1, -1}, {-1, 0}, {-1, 1}, {0, 1}, {1, 1},
            {1, 0}, {1, -1}, {0, -1}};

    /**
     * This is a method that returns the values of the neighboring cells of one specified cell.
     */

    private static List<Integer> getSurroundingCells(int[][] matrix, int row, int col) {
        List<Integer> surroundingCellsValues = new ArrayList<>();
        for (int[] direction : directions) {
            int coordinateRow = row + direction[1];
            int coordinateCol = col + direction[0];
            if (coordinateRow >= 0 && coordinateRow < matrix.length) {
                if (coordinateCol >= 0 && coordinateCol < matrix[coordinateRow].length) {
                    surroundingCellsValues.add(matrix[coordinateRow][coordinateCol]);
                }
            }
        }
        return surroundingCellsValues;
    }

    /**
     * This is a method that indicates whether a given red cell will turn into a green cell
     * in the next generation. It returns boolean which indicates if the cell will change its color.
     */

    public static boolean willBecomeGreenCell(int[][] matrix, int row, int col) {
        if (sum(getSurroundingCells(matrix, row, col)) == 2 || sum(getSurroundingCells(matrix, row, col)) == 5) {
            return true;
        }
        return false;
    }

    /**
     * This is a method that indicates whether a given green cell will turn into a red cell
     * in the next generation. It returns boolean which indicates if the cell will change its color.
     */

    public static boolean willBecomeRedCell(int[][] matrix, int row, int col) {
        if (sum(getSurroundingCells(matrix, row, col)) == 2 || sum(getSurroundingCells(matrix, row, col)) == 3 || sum(getSurroundingCells(matrix, row, col)) == 6) {
            return true;
        }
        return false;
    }

    /**
     * This is a method that sum all of the elements in a list.
     */

    public static int sum(List<Integer> list) {
        int sum = 0;
        for (int i : list) {
            sum += i;
        }
        return sum;
    }

}
