package com.company;

import java.util.Scanner;

/**
 * This is a class that shows a MentorMate's task solution.
 */

public class MentorMateTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int greenCell = 1;
        int redCell = 0;
        int numberCellIsGreen = 0;

        //Take the size of the matrix using an array and create the matrix.

        int[][] matrix = new int[0][];
        int[][] newMatrix = new int[0][];
        int rowCoordinate = 0;
        int colCoordinate = 0;
        int generationsInputNumber = 0;
        try {
            String[] initialSize = scanner.nextLine().split(", ");
            int width = Integer.parseInt(initialSize[0]);
            int height = Integer.parseInt(initialSize[1]);
            matrix = new int[height][width];
            newMatrix = new int [height][width];
            //Fill the matrix from the input using nested loop.

            for (int row = 0; row < matrix.length; row++) {
                String input = scanner.nextLine();
                for (int col = 0; col < matrix[row].length; col++) {
                    matrix[row][col] = Integer.parseInt(String.valueOf(input.charAt(col)));
                    newMatrix[row][col] = Integer.parseInt(String.valueOf(input.charAt(col)));
                }
            }
            //Take the coordinates of the cell and the number of generations from the input.

            String[] lastArguments = scanner.nextLine().split(", ");
            colCoordinate = Integer.parseInt(lastArguments[0]);
            rowCoordinate = Integer.parseInt(lastArguments[1]);
            generationsInputNumber = Integer.parseInt(lastArguments[lastArguments.length - 1]);
        } catch (NumberFormatException e) {
            throw new InvalidInputException();
        }

        /* This is a loop that changes the cells when the conditions about that are fulfilled.
        It also contains the number of generations in which they are green.*/
        for (int i = 0; i < generationsInputNumber; i++) {
            if(matrix[rowCoordinate][colCoordinate] == greenCell) {
                numberCellIsGreen++;
            }
            for (int row = 0; row < matrix.length; row++) {
                for (int col = 0; col < matrix[row].length; col++) {
                    if (matrix[row][col] == greenCell) {
                        if (Helper.willBecomeRedCell(matrix, row, col)) {
                            newMatrix[row][col] = redCell;
                        }
                    }

                    if (matrix[row][col] == redCell) {
                        if (Helper.willBecomeGreenCell(matrix, row, col)) {
                            newMatrix[row][col] = greenCell;
                        }
                    }

                }
            }
            for (int row = 0; row < matrix.length; row++){
                matrix[row] = newMatrix[row].clone();
            }
        }
        System.out.println(String.format("# expected result: %d", numberCellIsGreen));
    }

}

